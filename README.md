### Daniel Acedo Calderón ###

# Cuaderno de Clase con APISlim y Android #
Este ejercicio se basa en una combinación de implementar una API RESTful usando el framework Slim en PHP y a su vez crear una aplicación Android que se comunique con la API y manipular los datos.

La aplicación es un cuaderno de alumnos perteneciente a un profesor. Puede ver la información de todos sus alumnos, añadir nuevos, editarlos o eliminar. Además también tiene la posibilidad de enviarles emails con la información de cada alumno.

![1.PNG](https://bitbucket.org/repo/pxgxo8/images/1219044875-1.PNG)

![2.PNG](https://bitbucket.org/repo/pxgxo8/images/2492272075-2.PNG)

![3.PNG](https://bitbucket.org/repo/pxgxo8/images/3297688410-3.PNG)

![4.PNG](https://bitbucket.org/repo/pxgxo8/images/3802554060-4.PNG)

Al estar basado en API REST, los alumnos se recuperan de una base de datos remota situado en un servidor  mediante una petición HTTP y se reciben los datos de los alumnos almacenados en una base de datos MySQL.

El enrutado de direcciones está hecho con las herramientas que ofrece el framework SLIM, así creamos URL personalizadas que devolverán lo que nosotros queramos, en este caso los datos de la base de datos.

Las peticiones están protegidas con una clave que se pasa por la cabecera de la petición HTTP, ésta es comprobada por el servidor antes de atender la petición y dejará continuar o no.

Además de la gestión de alumnos la aplicación cuenta con otro modo en el que se puede señalar faltas de asistencia o calificaciones a los alumnos de la clase. Es posible visualizar sus datos y modificarlos para que se actualicen. Los cambios se harán efectivos al pulsar el botón de editar.

Las faltas que se muestran son de la fecha de hoy. Si no hay datos de un alumno en este día se autogeneraran con datos predeterminados para poder editarlos.

Los datos son:
- Falta: Justificada, Injustificada, Retraso, Ninguna
- Trabajo: Bien, regular, Mal.
- Comportamiento: Positivo, Negativo.
- Observacion: Comentario personal.

![5.PNG](https://bitbucket.org/repo/pxgxo8/images/3942767654-5.PNG)

![6.PNG](https://bitbucket.org/repo/pxgxo8/images/1486441287-6.PNG)

La principal carga del servidor esta en el archivo routes.php donde se mandan los datos dependiendo de la URL que llegue y el método HTTP.

Todos los archivos de apislim estan en el archivo apislim.zip y la base de datos está en cuaderno.sql lista para importar.