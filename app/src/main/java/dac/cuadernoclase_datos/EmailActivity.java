package dac.cuadernoclase_datos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import dac.cuadernoclase_datos.model.ResultStudent;
import dac.cuadernoclase_datos.model.Student;

public class EmailActivity extends AppCompatActivity {

    private static final String URL_EMAIL = "https://dani.alumno.club/email";
    public static final String KEY_EMAIL = "studentEmail";

    private EditText edt_emailSubject, edt_emailBody;
    private Button btn_sendEmail;

    private Student studentEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);

        edt_emailSubject = (EditText)findViewById(R.id.edt_email_Subject);
        edt_emailBody = (EditText)findViewById(R.id.edt_email_Body);
        btn_sendEmail = (Button)findViewById(R.id.btn_SendEmail);
        btn_sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });

        checkStudent();
    }

    private void checkStudent(){
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            studentEmail = bundle.getParcelable(KEY_EMAIL);
            setTitle("Email "+studentEmail.getEmail());
        }
    }

    private void sendEmail(){

        RequestParams params = new RequestParams();
        params.put("to",studentEmail.getEmail());
        params.put("subject", edt_emailSubject.getText().toString());
        params.put("message", edt_emailBody.getText().toString());

        RestClient.post(URL_EMAIL, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Gson gson = new Gson();
                ResultStudent result = gson.fromJson(response.toString(), ResultStudent.class);
                Toast.makeText(EmailActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(EmailActivity.this, "Error: "+throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
