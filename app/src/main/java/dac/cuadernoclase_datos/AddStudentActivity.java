package dac.cuadernoclase_datos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import dac.cuadernoclase_datos.model.ResultStudent;
import dac.cuadernoclase_datos.model.Student;

public class AddStudentActivity extends AppCompatActivity {
    public static final String KEY_EDIT = "editStudent";
    private static final String URL_ADD = "https://dani.alumno.club/student";

    private Button btn_addStudent;
    private EditText edt_studentName, edt_studentlastName, edt_studentAddress,
            edt_studentCity, edt_studentZipcode, edt_studentTelephone, edt_studentEmail;

    private Student editStudent;
    private boolean editMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        btn_addStudent = (Button)findViewById(R.id.btn_addStudent);
        btn_addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editMode){
                    editStudent();
                }else{
                    addStudent();
                }
            }
        });

        edt_studentName = (EditText) findViewById(R.id.edt_studentName);
        edt_studentlastName = (EditText)findViewById(R.id.edt_studentlastName);
        edt_studentAddress = (EditText)findViewById(R.id.edt_studentAddress);
        edt_studentCity = (EditText)findViewById(R.id.edt_studentCity);
        edt_studentZipcode = (EditText)findViewById(R.id.edt_studentZipcode);
        edt_studentTelephone = (EditText)findViewById(R.id.edt_studentTelephone);
        edt_studentEmail = (EditText)findViewById(R.id.edt_studentEmail);


        checkEditMode();
    }

    private void checkEditMode(){
        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            Student student = bundle.getParcelable(KEY_EDIT);

            if (student != null) {
                edt_studentName.setText(student.getName());
                edt_studentlastName.setText(student.getLastName());
                edt_studentAddress.setText(student.getAddress());
                edt_studentCity.setText(student.getCity());
                edt_studentZipcode.setText(student.getZipCode());
                edt_studentTelephone.setText(student.getTelephone());
                edt_studentEmail.setText(student.getEmail());

                btn_addStudent.setText("Editar");
                editMode = true;
                editStudent = student;
            }
        }
    }

    private void editStudent(){
        editStudent.setName(edt_studentName.getText().toString());
        editStudent.setLastName(edt_studentlastName.getText().toString());
        editStudent.setAddress(edt_studentAddress.getText().toString());
        editStudent.setCity(edt_studentCity.getText().toString());
        editStudent.setZipCode(edt_studentZipcode.getText().toString());
        editStudent.setTelephone(edt_studentTelephone.getText().toString());
        editStudent.setEmail(edt_studentEmail.getText().toString());


        RequestParams params = new RequestParams();
        params.put("name", editStudent.getName());
        params.put("lastname", editStudent.getLastName());
        params.put("address", editStudent.getAddress());
        params.put("city", editStudent.getCity());
        params.put("zipcode", editStudent.getZipCode());
        params.put("telephone", editStudent.getTelephone());
        params.put("email", editStudent.getEmail());

        RestClient.put(URL_ADD+"/"+String.valueOf(editStudent.get_id()), params ,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Gson gson = new Gson();
                ResultStudent result = gson.fromJson(response.toString(), ResultStudent.class);

                int status = result.getStatus();

                if (status == 200){
                    Toast.makeText(AddStudentActivity.this, "Entrada editada", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(AddStudentActivity.this, "Entrada no editada", Toast.LENGTH_SHORT).show();
                }

                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(AddStudentActivity.this, "Error al conectar con la API: "+throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addStudent(){
        String name = edt_studentName.getText().toString();
        String lastName = edt_studentlastName.getText().toString();
        String address = edt_studentAddress.getText().toString();
        String city = edt_studentCity.getText().toString();
        String zipCode = edt_studentZipcode.getText().toString();
        String telephone = edt_studentTelephone.getText().toString();
        String email = edt_studentEmail.getText().toString();

        RequestParams params = new RequestParams();
        params.put("name", name);
        params.put("lastname", lastName);
        params.put("address", address);
        params.put("city", city);
        params.put("zipcode", zipCode);
        params.put("telephone", telephone);
        params.put("email", email);

        RestClient.post(URL_ADD, params ,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Gson gson = new Gson();
                ResultStudent result = gson.fromJson(response.toString(), ResultStudent.class);

                int status = result.getStatus();

                if (status == 200){
                    Toast.makeText(AddStudentActivity.this, "Entrada creada", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(AddStudentActivity.this, "Entrada no creada", Toast.LENGTH_SHORT).show();
                }

                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(AddStudentActivity.this, "Error al conectar con la API: "+throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
