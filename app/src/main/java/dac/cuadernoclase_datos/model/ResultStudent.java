package dac.cuadernoclase_datos.model;

import java.util.List;

/**
 * Created by Daniel on 04/03/2017.
 */

public class ResultStudent {
    private boolean code;
    private int status;
    private String message;
    private List<Student> student;
    private int last;

    public ResultStudent(boolean code, int status, String message, List<Student> student, int last) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.student = student;
        this.last = last;
    }

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Student> getStudent() {
        return student;
    }

    public void setStudent(List<Student> student) {
        this.student = student;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }
}
