package dac.cuadernoclase_datos;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import dac.cuadernoclase_datos.adapter.StudentAdapter;
import dac.cuadernoclase_datos.model.ResultStudent;
import dac.cuadernoclase_datos.model.Student;

public class StudentActivity extends AppCompatActivity {

    private static final String STUDENTS_URL = "https://dani.alumno.club/student";

    private ListView lv_students;
    private StudentAdapter adapter;
    private FloatingActionButton fab_addStudent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_acitivty);

        setTitle("Alumnos");

        lv_students = (ListView)findViewById(R.id.listStudent);
        registerForContextMenu(lv_students);

        fab_addStudent = (FloatingActionButton)findViewById(R.id.fab_addStudent);
        fab_addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StudentActivity.this, AddStudentActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadStudents();
    }

    private void loadStudents(){
        RestClient.get(STUDENTS_URL, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Gson gson = new Gson();
                    ResultStudent result = gson.fromJson(response.toString(), ResultStudent.class);

                    adapter = new StudentAdapter(StudentActivity.this, result.getStudent());
                    lv_students.setAdapter(adapter);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(StudentActivity.this, "Error al conectar con la API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteStudent(final Student student){
        RequestParams requestParams = new RequestParams();


        RestClient.delete(STUDENTS_URL+"/"+String.valueOf(student.get_id()), requestParams ,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Gson gson = new Gson();
                ResultStudent result = gson.fromJson(response.toString(), ResultStudent.class);
                
                if (result.getStatus() == 200){
                    Toast.makeText(StudentActivity.this, "Entrada borrada", Toast.LENGTH_SHORT).show();
                    adapter.remove(student);
                    adapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(StudentActivity.this, "Entrada no borrada", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(StudentActivity.this, "Error al conectar con la API: "+throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(StudentActivity.this, "Error al conectar con la API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getMenuInflater().inflate(R.menu.ctx_student_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuinfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.ctx_student_delete:
                deleteStudent(adapter.getItem(menuinfo.position));
                break;
            case R.id.ctx_student_edit:
                Bundle bundle = new Bundle();
                bundle.putParcelable(AddStudentActivity.KEY_EDIT, adapter.getItem(menuinfo.position));

                Intent intent = new Intent(StudentActivity.this, AddStudentActivity.class);
                intent.putExtras(bundle);

                startActivity(intent);

                break;

            case R.id.ctx_student_email:
                bundle = new Bundle();
                bundle.putParcelable(EmailActivity.KEY_EMAIL, adapter.getItem(menuinfo.position));

                intent = new Intent(StudentActivity.this, EmailActivity.class);
                intent.putExtras(bundle);

                startActivity(intent);
                break;
        }

        return super.onContextItemSelected(item);
    }
}
