package dac.cuadernoclase_datos.model;

import android.support.annotation.Nullable;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by usuario on 9/02/17.
 */

public class IncidenceData {
    /** VALUES DEFINITION **/

    @Retention(RetentionPolicy.SOURCE)
    @StringDef()
    public @interface AbsenceValues{
        String justified = "J";
        String unjustified = "I";
        String delay = "R";
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef()
    public @interface WorkValues{
        String good = "B";
        String soso = "R";
        String bad = "M";
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef()
    public @interface BehaviourValues{
        String positive = "P";
        String negative = "N";
    }


    /** VARIABLES **/
    private @AbsenceValues String absence; //Grade of absence
    private @WorkValues String work; //Work evaluation
    private @BehaviourValues String behaviour;
    private String observation;

    public IncidenceData(@AbsenceValues String absence, @WorkValues String work,
                         @BehaviourValues String behaviour, @Nullable String observation) {

        this.absence = absence;
        this.work = work;
        this.behaviour = behaviour;
        this.observation = observation;
    }


    public String getAbsence() {
        return absence;
    }

    public void setAbsence(@AbsenceValues String absence) {
        this.absence = absence;
    }

    public String getWork() {
        return work;
    }

    public void setWork(@WorkValues String work) {
        this.work = work;
    }

    public String getBehaviour() {
        return behaviour;
    }

    public void setBehaviour(@BehaviourValues String behaviour) {
        this.behaviour = behaviour;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
}
