package dac.cuadernoclase_datos;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.joda.time.DateTime;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.annotation.ThreadSafe;
import dac.cuadernoclase_datos.adapter.IncidenceAdapter;
import dac.cuadernoclase_datos.adapter.StudentAdapter;
import dac.cuadernoclase_datos.model.Incidence;
import dac.cuadernoclase_datos.model.ResultIncidence;
import dac.cuadernoclase_datos.model.ResultStudent;
import dac.cuadernoclase_datos.model.Student;

public class IncidenceActivity extends AppCompatActivity {

    private static final String STUDENTS_URL = "https://dani.alumno.club/student";
    public static final String URL_INCIDENCES = "https://dani.alumno.club/incidence";

    private ViewGroup header;
    private ListView lv_incidences;
    private IncidenceAdapter adapter;

    private FloatingActionButton fab_editIncidence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incidence);

        DateTime today = DateTime.now();

        setTitle(today.dayOfWeek().getAsText()+", "+today.getDayOfMonth()+"-"+today.getMonthOfYear()+"-"+today.getYear());

        header = (ViewGroup)findViewById(R.id.header);
        header.addView(LayoutInflater.from(this).inflate(R.layout.item_incidence_header, null));

        lv_incidences = (ListView)findViewById(R.id.lv_incidences);

        fab_editIncidence = (FloatingActionButton)findViewById(R.id.fab_modifyIncidence);
        fab_editIncidence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapter != null){
                    Gson gson = new Gson();
                    String json = gson.toJson(adapter.getList());
                    editIncidences(json);
                }
            }
        });

        loadIncidences();
    }


    private void editIncidences(String json){
        RequestParams req = new RequestParams();
        req.add("incidences", json);

        RestClient.post(URL_INCIDENCES, req, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Gson gson = new Gson();
                ResultStudent result = gson.fromJson(response.toString(), ResultStudent.class);

                Toast.makeText(IncidenceActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } );
    }

    private void loadIncidences(){
        RestClient.get(URL_INCIDENCES, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Gson gson = new Gson();

                ResultIncidence resultIncidence = gson.fromJson(response.toString(), ResultIncidence.class);

                loadStudents(resultIncidence.getIncidence());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(IncidenceActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadStudents(final List<Incidence> incidences){
        RestClient.get(STUDENTS_URL, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Gson gson = new Gson();
                ResultStudent result = gson.fromJson(response.toString(), ResultStudent.class);


                doList(result.getStudent(), incidences);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(IncidenceActivity.this, "Error al conectar con la API", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void doList(List<Student> students, List<Incidence> incidences){
        List<Student> studentsInIncidence = new ArrayList<>();

        //Create a list of students in incidences
        if (students != null && incidences != null){
            for (Incidence incidence : incidences){
                studentsInIncidence.add(incidence.getStudent());
            }
        }

        //Students who doesn't have a incidence already gets a default one
        for (Student student : students){
            if(!studentsInIncidence.contains(student)){
                incidences.add(new Incidence(
                        student,
                        DateTime.now().toString("y/M/d"),
                        Incidence.AbsenceValues.no,
                        Incidence.WorkValues.good,
                        Incidence.BehaviourValues.positive,
                        ""
                ));
            }
        }

        adapter = new IncidenceAdapter(IncidenceActivity.this, incidences);
        lv_incidences.setAdapter(adapter);
    }
}

