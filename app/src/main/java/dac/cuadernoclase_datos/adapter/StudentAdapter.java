package dac.cuadernoclase_datos.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dac.cuadernoclase_datos.R;
import dac.cuadernoclase_datos.model.Student;

/**
 * Created by Daniel on 04/03/2017.
 */

public class StudentAdapter extends ArrayAdapter<Student> {

    public StudentAdapter(Context context, List<Student> list){
        super(context, R.layout.student_item, new ArrayList<Student>(list));
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        StudentHolder holder = null;

        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.student_item, null);
            holder = new StudentHolder();

            holder.txv_studentName = (TextView)v.findViewById(R.id.txv_studentName);
            holder.txv_studentAddress = (TextView)v.findViewById(R.id.txv_studentAddress);
            holder.txv_studentEmail = (TextView)v.findViewById(R.id.studentEmail);
            holder.txv_studentTelephone = (TextView)v.findViewById(R.id.studentTelephone);

            v.setTag(holder);
        }else{
            holder = (StudentHolder)v.getTag();
        }

        Student student = getItem(position);

        holder.txv_studentName.setText(student.getName()+" "+ student.getLastName());
        holder.txv_studentAddress.setText(student.getAddress()+", "+student.getZipCode()+" "+student.getCity());
        holder.txv_studentEmail.setText(student.getEmail());
        holder.txv_studentTelephone.setText(student.getTelephone());

        return v;
    }

    public static class StudentHolder{
        private TextView txv_studentName, txv_studentAddress, txv_studentEmail, txv_studentTelephone;
    }
}
