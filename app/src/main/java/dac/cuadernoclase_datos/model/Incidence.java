package dac.cuadernoclase_datos.model;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by usuario on 9/02/17.
 */

public class Incidence {
    /** VALUES DEFINITION **/

    @Retention(RetentionPolicy.SOURCE)
    @StringDef()
    public @interface AbsenceValues{
        String justified = "J";
        String unjustified = "I";
        String delay = "R";
        String no = "N";
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef()
    public @interface WorkValues{
        String good = "B";
        String soso = "R";
        String bad = "M";
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef()
    public @interface BehaviourValues{
        String positive = "P";
        String negative = "N";
    }


    /** VARIABLES **/
    private @IncidenceData.AbsenceValues
    String absence; //Grade of absence
    private @IncidenceData.WorkValues
    String work; //Work evaluation
    private @IncidenceData.BehaviourValues
    String behaviour;

    private Student student;

    private String observation;

    private String date;


    public Incidence(String date, @AbsenceValues String absence, @WorkValues String work, @BehaviourValues String behaviour, String observation) {
        this.absence = absence;
        this.work = work;
        this.behaviour = behaviour;
        this.observation = observation;
        this.date = date;
    }

    public Incidence(Student student, String date, String absence, String work, String behaviour, String observation) {
        this.absence = absence;
        this.work = work;
        this.behaviour = behaviour;
        this.student = student;
        this.observation = observation;
        this.date = date;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAbsence() {
        return absence;
    }

    public void setAbsence(String absence) {
        this.absence = absence;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getBehaviour() {
        return behaviour;
    }

    public void setBehaviour(String behaviour) {
        this.behaviour = behaviour;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
}
