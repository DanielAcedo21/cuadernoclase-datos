package dac.cuadernoclase_datos.model;

import java.util.List;

/**
 * Created by Daniel on 05/03/2017.
 */

public class ResultIncidence {
    private boolean code;
    private int status;
    private String message;
    private List<Incidence> incidence;
    private int last;



    public ResultIncidence(){}

    public List<Incidence> getIncidence() {
        return incidence;
    }

    public void setIncidence(List<Incidence> incidence) {
        this.incidence = incidence;
    }

    public boolean isCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }
}

