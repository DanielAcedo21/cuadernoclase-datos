package dac.cuadernoclase_datos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by usuario on 9/02/17.
 */

public class Student implements Parcelable {
    private int _id;
    private String name;
    @SerializedName("lastname")
    private String lastName;
    private String address;
    private String city;
    @SerializedName("zipcode")
    private String zipCode;
    private String telephone;
    private String email;

    public Student(){}

    public Student(String name, String lastName, String address,
                   String city, String zipCode, String telephone,
                   String email) {
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.telephone = telephone;
        this.email = email;
    }

    protected Student(Parcel in) {
        _id = in.readInt();
        name = in.readString();
        lastName = in.readString();
        address = in.readString();
        city = in.readString();
        zipCode = in.readString();
        telephone = in.readString();
        email = in.readString();
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeString(name);
        dest.writeString(lastName);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(zipCode);
        dest.writeString(telephone);
        dest.writeString(email);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Student){
            Student s = (Student)obj;

            if(s.get_id() == this.get_id()){
                return true;
            }
        }

        return false;
    }
}
