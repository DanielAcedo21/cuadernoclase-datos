-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-03-2017 a las 18:36:53
-- Versión del servidor: 5.7.17-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cuadernoDBdaniel`
--
CREATE DATABASE IF NOT EXISTS `cuadernoDBdaniel` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `cuadernoDBdaniel`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `absenceTypes`
--

CREATE TABLE `absenceTypes` (
  `_id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `absenceTypes`
--

INSERT INTO `absenceTypes` (`_id`, `name`) VALUES
(1, 'J'),
(2, 'I'),
(3, 'R'),
(4, 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `behaviourType`
--

CREATE TABLE `behaviourType` (
  `_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `behaviourType`
--

INSERT INTO `behaviourType` (`_id`, `name`) VALUES
(1, 'P'),
(2, 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidence`
--

CREATE TABLE `incidence` (
  `_id` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `date` date NOT NULL,
  `id_absence` int(11) NOT NULL,
  `id_behaviour` int(11) NOT NULL,
  `id_work` int(11) NOT NULL,
  `observation` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `incidence`
--

INSERT INTO `incidence` (`_id`, `id_student`, `date`, `id_absence`, `id_behaviour`, `id_work`, `observation`) VALUES
(11, 12, '2017-03-20', 1, 1, 1, 'Esto no es de hoy'),
(30, 1, '2017-03-05', 1, 1, 1, 'Buena'),
(31, 12, '2017-03-05', 2, 1, 2, 'Regular'),
(32, 13, '2017-03-05', 3, 1, 1, 'Editado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE `student` (
  `_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `zipcode` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telephone` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(70) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `student`
--

INSERT INTO `student` (`_id`, `name`, `lastname`, `address`, `city`, `zipcode`, `telephone`, `email`) VALUES
(1, 'Daniel', 'Acedo Calderón', 'C/Carretería 4', 'Málaga', '29010', '657984323', 'dani_acedo@hotmail.com'),
(12, 'Sandra', 'Ramos', 'C/ Picasso 23', 'Malaga', '29010', '687231245', 'dani_acedo@hotmail.com'),
(13, 'José', 'Romero', 'C/Picaporte', 'Málaga', '29010', '987236387', 'dani_acedo@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `workTypes`
--

CREATE TABLE `workTypes` (
  `_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `workTypes`
--

INSERT INTO `workTypes` (`_id`, `name`) VALUES
(1, 'B'),
(2, 'R'),
(3, 'N');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `absenceTypes`
--
ALTER TABLE `absenceTypes`
  ADD PRIMARY KEY (`_id`);

--
-- Indices de la tabla `behaviourType`
--
ALTER TABLE `behaviourType`
  ADD PRIMARY KEY (`_id`);

--
-- Indices de la tabla `incidence`
--
ALTER TABLE `incidence`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `id_student` (`id_student`),
  ADD KEY `id_absence` (`id_absence`),
  ADD KEY `id_behaviour` (`id_behaviour`),
  ADD KEY `id_work` (`id_work`);

--
-- Indices de la tabla `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`_id`);

--
-- Indices de la tabla `workTypes`
--
ALTER TABLE `workTypes`
  ADD PRIMARY KEY (`_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `absenceTypes`
--
ALTER TABLE `absenceTypes`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `behaviourType`
--
ALTER TABLE `behaviourType`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `incidence`
--
ALTER TABLE `incidence`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `student`
--
ALTER TABLE `student`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `workTypes`
--
ALTER TABLE `workTypes`
  MODIFY `_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `incidence`
--
ALTER TABLE `incidence`
  ADD CONSTRAINT `absenceFK` FOREIGN KEY (`id_absence`) REFERENCES `absenceTypes` (`_id`),
  ADD CONSTRAINT `behaviourFK` FOREIGN KEY (`id_behaviour`) REFERENCES `behaviourType` (`_id`),
  ADD CONSTRAINT `studentFK` FOREIGN KEY (`id_student`) REFERENCES `student` (`_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `workFK` FOREIGN KEY (`id_work`) REFERENCES `workTypes` (`_id`);