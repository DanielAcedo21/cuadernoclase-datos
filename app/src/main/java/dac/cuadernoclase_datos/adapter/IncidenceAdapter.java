package dac.cuadernoclase_datos.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dac.cuadernoclase_datos.R;
import dac.cuadernoclase_datos.model.Incidence;

/**
 * Created by Daniel on 05/03/2017.
 */

public class IncidenceAdapter extends ArrayAdapter<Incidence> {

    List<String> absenceList;
    List<String> workList;
    List<String> behaviourList;

    ArrayAdapter adapterAbsence;
    ArrayAdapter adapterWork;
    ArrayAdapter adapterBehaviour;

    Map<String, Integer> absenceMap;
    Map<String, Integer> workMap;
    Map<String, Integer> behaviourMap;

    List<Incidence> mList;


    public IncidenceAdapter(Context context, List<Incidence> incidences){
        super(context, R.layout.item_incidence, incidences);

        mList = incidences;
        initializeAbsence();
        initializeWork();
        initializeBehaviour();
    }

    private void initializeAbsence(){
        absenceList = new ArrayList<>();
        absenceList.add(Incidence.AbsenceValues.no);
        absenceList.add(Incidence.AbsenceValues.justified);
        absenceList.add(Incidence.AbsenceValues.unjustified);
        absenceList.add(Incidence.AbsenceValues.delay);

        absenceMap = new HashMap<>();
        absenceMap.put(Incidence.AbsenceValues.no, 0);
        absenceMap.put(Incidence.AbsenceValues.justified, 1);
        absenceMap.put(Incidence.AbsenceValues.unjustified, 2);
        absenceMap.put(Incidence.AbsenceValues.delay, 3);


        adapterAbsence = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, absenceList);
    }

    private void initializeWork(){
        workList = new ArrayList<>();
        workList.add(Incidence.WorkValues.good);
        workList.add(Incidence.WorkValues.soso);
        workList.add(Incidence.WorkValues.bad);

        workMap = new HashMap<>();
        workMap.put(Incidence.WorkValues.good, 0);
        workMap.put(Incidence.WorkValues.soso, 1);
        workMap.put(Incidence.WorkValues.bad, 2);

        adapterWork = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, workList);
    }

    private void initializeBehaviour(){
        behaviourList = new ArrayList<>();
        behaviourList.add(Incidence.BehaviourValues.positive);
        behaviourList.add(Incidence.BehaviourValues.negative);

        behaviourMap = new HashMap<>();
        behaviourMap.put(Incidence.BehaviourValues.positive, 0);
        behaviourMap.put(Incidence.BehaviourValues.negative, 1);

        adapterBehaviour = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, behaviourList);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        IncidenceHolder holder = null;

        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.item_incidence, null);
            holder = new IncidenceHolder();

            holder.txv_student = (TextView) v.findViewById(R.id.txv_student);
            holder.edt_observation = (EditText) v.findViewById(R.id.edt_observation);
            holder.spn_absence = (Spinner)v.findViewById(R.id.spn_absence);
            holder.spn_work = (Spinner)v.findViewById(R.id.spn_work);
            holder.spn_behaviour = (Spinner)v.findViewById(R.id.spn_behaviour);


            v.setTag(holder);
        }else{
            holder = (IncidenceHolder)v.getTag();
        }

        final Incidence incidence = getItem(position);



        holder.txv_student.setText(incidence.getStudent().getName()+" "+incidence.getStudent().getLastName());

        holder.edt_observation.setText(incidence.getObservation());
        holder.edt_observation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                incidence.setObservation(s.toString());
            }
        });

        holder.spn_absence.setAdapter(adapterAbsence);
        holder.spn_work.setAdapter(adapterWork);
        holder.spn_behaviour.setAdapter(adapterBehaviour);

        holder.spn_absence.setSelection(absenceMap.get(incidence.getAbsence()));
        holder.spn_work.setSelection(workMap.get(incidence.getWork()));
        holder.spn_behaviour.setSelection(behaviourMap.get(incidence.getBehaviour()));

        holder.spn_absence.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                incidence.setAbsence(absenceList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.spn_work.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                incidence.setWork(workList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.spn_behaviour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                incidence.setBehaviour(behaviourList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return v;
    }

    public List<Incidence> getList(){
        return mList;
    }

    public static class IncidenceHolder{
        TextView txv_student;
        EditText edt_observation;
        TextWatcher watcher;
        Spinner spn_absence, spn_work, spn_behaviour;
    }
}
